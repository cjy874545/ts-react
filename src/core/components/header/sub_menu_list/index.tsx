import * as React from 'react';
import subMenuList from './index.scss';
import className from 'classnames/bind';

const c = className.bind(subMenuList);

interface Props {
    displayState: boolean,
    subtitle: string,
    submenu: [
        {
            title: string,
            list: any[]
        }

    ]
}
const SubMenuList: React.FC<Props> = ({ displayState, submenu, subtitle }) => {
    return (
            <div className={c('sub-menu-list-container', { 'mouseleave-event': !displayState })} >
                <div className={c('container-text-box')}>
                    <div className={c('left-sub-box')}>
                        <div className={c('left-sub-box__header')}>{subtitle}</div>
                        <div className={c('left-sub-box__body')}>
                            안전한 보안세상을 만드는
                            테르텐의 보안 솔루션을 소개합니다.
                        </div>
                    </div>
                </div>
                <div className={c('sub-menu-list')}>
                    <div className={c('right-sub-box')}>
                        {
                            submenu.map((item: any, index: number) => {
                                return (
                                    <div className={c('sub-menu-list__inner--wrapper')} key={index}>
                                        <div className={c('sub-menu-list__inner')}>
                                            <ul>
                                                <li className={c('sub-menu-list-contents')}>
                                                    <div className={c('sub-title')}>
                                                        {item.title}
                                                    </div>
                                                    <ul className={c('sub-area')} key={item.title}>
                                                        {
                                                            item.list.length > 0 &&
                                                            item.list.map((subItem: any, subIndex: number) => {
                                                                return (<li key={subIndex}>{subItem}</li>)
                                                            })
                                                        }
                                                    </ul>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
                <div className={c('test')}/>
            </div>
            
    );
}
export default SubMenuList;