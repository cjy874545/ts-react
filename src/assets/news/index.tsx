import news1 from './news1.png';
import news2 from './news2.png';
import news3 from './news3.png';
import news4 from './news4.png';
import news5 from './news5.png';
import news6 from './news6.png';
import news7 from './news7.png';
import news8 from './news8.png';
import news9 from './news9.png';
import news10 from './news10.png';
import news11 from './news11.png';
import news12 from './news12.png';
import news13 from './news13.png';
import news14 from './news14.png';
import googlekorea from './googlekorea.png';


const news: any[] = [
    news1,
    news2,
    news3,
    news4,
    news5,
    news6,
    news7,
    news8,
    news9,
    news10,
    news11,
    news12,
    news13,
    news14,
    googlekorea,
];

export default news;