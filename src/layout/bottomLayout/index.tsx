import * as React from 'react';
import bottomLayout from './index.scss';
import className from 'classnames/bind';
import logo from 'assets/moornmo.png';
import banner1 from 'assets/mac1.png'
import banner2 from 'assets/mac2.jpg'

const c = className.bind(bottomLayout);

interface Menu {
    listInfo: any[]
}

class BottomLayout extends React.Component {

    public menu: Menu = {
        listInfo: [{
            title: 'Sonlution & Service',
            path: '/solution',
            list: [{
                title: 'Solution Map',
                list: ['test1','tes2t','test3','test4','test111111']
            }, {
                title: 'SaaS 솔루션',
                list: []
            }, {
                title: '데이터 보안',
                list: ['화면 보안', '모바일 보안', '멀티미디어 보안', 'PC 영역 보안']
            }, {
                title: '보안 관제',
                list: ['웹 사이트 보안']
            }]
        }, {
            title: 'CustomerCenter',
            path: '/customer',
            list: [{
                title: '제품문의',
                list: []
            }, {
                title: 'Contact Us',
                list: []
            }]
        }, {
            title: 'PR',
            path: '/pr',
            list: [{
                title: '뉴스 및 공지',
                list: []
            }, {
                title: '뉴스레더',
                list: []
            }, {
                title: '광고',
                list: []
            }, {
                title: '도입사례',
                list: []
            }, {
                title: '사내소식',
                list: []
            }]
        }, {
            title: 'Company',
            path: '/company',
            list: [{
                title: '회사소개',
                list: []
            }, {
                title: 'CEO메시지',
                list: []
            }, {
                title: 'Award',
                list: []
            }, {
                title: '인재채용',
                list: []
            }, {
                title: '오시는 길',
                list: []
            }]
        }]
    }

    public render() {
        return (
            <footer className={c('footer')}>
                <div className={c('footer__banner')}>
                    <div className={c('wrapper')}>
                        <div className={c('left-banner')}>
                            <div className={c('banner__title')}>MAC</div>
                            <picture>
                                {/* <source media="(min-witdh: 900px)" srcset={}/> */}
                                <img src={banner1} alt="banner1" />
                            </picture>
                        </div>
                        <div className={c('right-banner')}>
                        <div className={c('banner__title')}>MAC</div>
                            <picture>
                                {/* <source media="(min-witdh: 900px)" srcset={}/> */}
                                <img src={banner2} alt="banner1" />
                            </picture>
                        </div>
                    </div>
                </div>
                <div className={c('footer__site-map')}>
                    <div className={c('wrapper')}>
                        {
                            this.menu.listInfo.map((item, index) => {
                                return (
                                    <div className={c('site-container')} key={index}>
                                        <div className={c('title')}>{item.title}</div>
                                        <div className={c('ul-wrapper')}>
                                            <ul>
                                                {
                                                    item.list.map((listItem: any) => {
                                                        return (
                                                            <li key={listItem.title}>
                                                                {listItem.title}
                                                                <ul>
                                                                    {
                                                                        listItem.list.map((listItem2: any) => {
                                                                            return (
                                                                                <li key={listItem2}>{listItem2}</li>
                                                                            );
                                                                        })
                                                                    }
                                                                </ul>
                                                            </li>
                                                        );

                                                    })
                                                }

                                            </ul>
                                        </div>
                                    </div>
                                );
                            })
                        }

                    </div>
                </div>
                <div className={c('footer__comopay-info')}>
                    <div className={c('wrapper')}>
                        <div className={c('header')}>
                            <img src={logo} alt="회사로고" />
                            <div><a href="/">저작권보호정책</a></div>
                            <div><a href="/">개인정보처리방침</a></div>
                            <div><a href="/">이메일무단수집거부</a></div>
                        </div>
                        <div className={c('body')}>
                            <div className={c('address')}>부산광역시 도레미파솔라시도!!</div>
                            <div>0838whenw ahensdhanwhd ajwsjad saklwjakw djwaTEL :00291010192901</div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default BottomLayout;