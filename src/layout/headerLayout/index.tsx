import * as React from 'react';
import header from './index.scss';
import className from 'classnames/bind';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import SubMenuList from 'core/components/header/sub_menu_list';
import logo from 'assets/moornmo.png';
// RouteComponentProps
const c = className.bind(header);

interface Menu {
    listInfo: any[],
}


const HeaderLayout: React.FC = () => {
    const menu: Menu = {
        listInfo: [{
            title: 'Sonlution & Service',
            path: '/solution',
            list: [{
                title: 'Solution Map',
                list: []
            }, {
                title: 'SaaS 솔루션',
                list: []
            }, {
                title: '데이터 보안',
                list: ['화면 보안', '모바일 보안', '멀티미디어 보안', 'PC 영역 보안', 'PC 영역 보안', 'PC 영역 보안', 'PC 영역 보안', 'PC 영역 보안']
            }, {
                title: '보안 관제',
                list: ['웹 사이트 보안']
            }],
            show: false
        }, {
            title: 'CustomerCenter',
            path: '/customer',
            list: [{
                title: '제품문의',
                list: []
            }, {
                title: 'Contact Us',
                list: []
            }],
            show: false
        }, {
            title: 'PR',
            path: '/pr',
            list: [{
                title: '뉴스 및 공지',
                list: []
            }, {
                title: '뉴스레더',
                list: []
            }, {
                title: '광고',
                list: []
            }, {
                title: '도입사례',
                list: []
            }, {
                title: '사내소식',
                list: []
            }],
            show: false
        }, {
            title: 'Company',
            path: '/company',
            list: [{
                title: '회사소개',
                list: []
            }, {
                title: 'CEO메시지',
                list: []
            }, {
                title: 'Award',
                list: []
            }, {
                title: '인재채용',
                list: []
            }, {
                title: '오시는 길',
                list: []
            }],
            show: false
        }]
    };

    const [menuList, setShow] = React.useState(menu.listInfo);
    const [backCoverState, setCover] = React.useState(false);


    const submenuShow = (menuIndex: number, state: boolean) => {
        return (event: React.MouseEvent<HTMLLIElement>) => {
            const changeMenuListShow: any[] = menuList.map((item, index) => {
                if (index === menuIndex) {
                    item.show = state;
                }
                setCover(state);
                return item;
            });
            setShow(changeMenuListShow);
        }
    }
    return (

        <header className={c('header')}>
            <Router>

                <div className={c('header__inner')}>

                    <div className={c('logo')}>
                        <Link to="/"><img src={logo} alt="회사로고" /></Link></div>
                    <nav>
                        <ul className={c('area')}>
                            {

                                menuList.map((item, menuIndex) => {
                                    return (
                                        <li className={c('area-list')} key={item.title}
                                            onMouseOver={submenuShow(menuIndex, true)}
                                            onMouseLeave={submenuShow(menuIndex, false)}
                                        >
                                            <Link to={item.path}>{item.title}</Link>
                                            <SubMenuList displayState={item.show}
                                                submenu={item.list}
                                                subtitle={item.title}
                                            />
                                        </li>
                                    )
                                })
                            }
                        </ul>

                    </nav>
                </div>
            </Router>
            <div className={c('background-cover', { 'display-none': !backCoverState })} />
        </header>
    );
};

export default HeaderLayout;
