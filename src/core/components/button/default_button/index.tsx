import * as React from 'react';
import ButtonCss from './index.scss';
import className from 'classnames/bind'

const c = className.bind(ButtonCss);

interface Props {
    buttonName: string
}

const DefaultButton: React.FC<Props> = ({ buttonName }) => {
    return (
        <div className={c('button-container')}>
            <button type="button" className={c('button')}>{buttonName}</button>
        </div>
    );
}

export default DefaultButton;