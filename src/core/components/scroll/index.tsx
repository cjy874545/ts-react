import * as React from 'react';
import scroll from 'index.scss';
import ClassName from 'classnames/bind';

const c = ClassName.bind(scroll);
const Scroll: React.FC = () => {
    return(
        <div className={c('scroll-container')}>
            scroll
        </div>
    );
}

export default Scroll;