import * as React from 'react';
import ProductCardCss from './index.scss';
import className from 'classnames/bind';
import DefaultButton from 'core/components/button/default_button/';

const c = className.bind(ProductCardCss);
interface Props {
    list: {
        index: number,
        icon: any,
        title: string,
        button: any
    }
}
const ProductCard: React.FC<Props> = ({ list }) => {
    return (
        <div className={c('product-card-container')}>
            <div className={c('product-card__header')}>
                <i className={c('material-icons', 'custom-icons')}>{list.icon}</i>
            </div>
            <div className={c('product-card__body')}>
                {list.title}
            </div>
            <div className={c('product-card__footer')}>
                <DefaultButton buttonName={list.button} />
            </div>
        </div>
    );
}

export default ProductCard;