import * as React from 'react';
import mainPage from './index.scss';
import className from 'classnames/bind';
import ProductCard from 'core/components/card/product-card/';
import Viewer from 'core/components/viewer/';
import news from 'src/assets/news/';

const c = className.bind(mainPage);

interface Card {
    list: any[]
}

class MainPage extends React.Component {

    public card: Card = {
        list: [
            {
                index: 0,
                icon: 'brightness_4',
                title: 'Solution',
                button: 'Read More'
            },
            {
                index: 1,
                icon: 'free_breakfast',
                title: '화면 보안 솔루션WebCube',
                button: 'Read More'
            },
            {
                index: 2,
                icon: 'layers',
                title: '멀티 미디어 DRM',
                button: 'Read More'
            },
            {
                index: 3,
                icon: 'security',
                title: 'SaaS 보안 솔루션',
                button: 'Read More'
            }
        ]
    }

    public render() {
        return (
            <div className={c('main-container')}>
                <div className={c('main-visual')} />
                <div className={c('main-content')}>
                    <div className={c('product-introduce-container')}>
                        <div className={c('product-introduce__title')}>
                            제품소개
                        </div>
                        <div className={c('product-introduce__contents')}>
                            {
                                this.card.list.map((item) => {
                                    return (<ProductCard list={item} key={item.index} />);
                                })
                            }
                        </div>
                    </div>
                    <article className={c('news-container')}>
                        <div className={c('area')}>
                            <div className={c('area__main')}>
                                <Viewer viewMode={'normal'} items={news}/>
                            </div>
                            <div className={c('area__sub')}>
                                <div className={c('top')}>
                                    title
                                </div>
                                <div className={c('bottom')}>
                                    <Viewer viewMode={'scroll'} items={news} width={200}/>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        );
    }
}

export default MainPage;