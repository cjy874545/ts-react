FROM node:11.13.0

WORKDIR /ts-react

COPY . .

RUN npm install
RUN npm rebuild node-sass

CMD ["yarn","start"]