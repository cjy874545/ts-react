import * as React from 'react';
import viewer from './index.scss';
import className from 'classnames/bind';

const c = className.bind(viewer);

interface Props {
    width?: number,
    viewMode: string,
    items: any
}

const Viewer: React.FC<Props> = ({ width, viewMode, items }) => {
    const [scrollStartX, setScrollStartX] = React.useState(0);
    const [scrollLeft, setScrollLeft] = React.useState(0);
    const [mouseDownMaintain, setMouseDownMaintain] = React.useState(false);
    const [isBigSize, setIsBigSize] = React.useState(false);
    const [itemList, setItemList] = React.useState(items)
    const containerRef: any = React.useRef();
    const testRef: any = React.useRef();


    const scrollDown = (target: any) => {
        return (event: React.MouseEvent): void => {
            setScrollStartX(event.pageX - target.current.offsetLeft);
            setScrollLeft(target.current.scrollLeft);


            setMouseDownMaintain(true);
            setIsBigSize(true);
            console.log(testRef)
            event.preventDefault();
        }
    }

    const scrollUp = (): void => {
        setMouseDownMaintain(false);
    }

    const scrollDrag = (target: any) => {
        return (event: React.MouseEvent<HTMLDivElement>): void => {
            if (mouseDownMaintain) {
                const value: number = (event.pageX - target.current.offsetLeft) - scrollStartX;
                target.current.scrollLeft = (scrollLeft - value);
                console.log(setItemList);
                const changeItemList = itemList.map((item: any, index: number) => {
                    return item;
                });
                console.log(changeItemList)
                setItemList(changeItemList);
                event.preventDefault();
            }
        }
    }

    return (
        <div className={c('viewer-container')}>

            {
                viewMode === 'scroll' ?
                    (
                        <div className={c('viewer-wrapper')}
                            style={{ width: `calc(${width}px * 3)` }}>
                            <div className={c('viewer', 'scroll')}
                                onMouseMove={scrollDrag(containerRef)}
                                onMouseDown={scrollDown(containerRef)}
                                onMouseUp={scrollUp}
                                onMouseLeave={scrollUp}
                                ref={containerRef}
                                style={{ width: `calc(${width}px * 9)` }}>
                                {

                                    itemList.map((item: any, index: number) => {
                                        return (
                                            <div className={c('image-wrapper', { 'big-size-view': isBigSize })}
                                                tabIndex={index}
                                                style={{ width: `${width}px` }} key={item}
                                                ref={testRef}>
                                                <img src={item} alt="img" />
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    ) : (
                        <div className={c('viewer')}>
                            <div className={c('image-wrapper')}>
                                <img src={items[1]} alt="img" />
                            </div>
                        </div>
                    )
            }
        </div>


    );
}

export default Viewer;